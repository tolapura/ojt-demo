#Question 1 (Level 1)
def question_1():
    res = ""
    for i in range(2000, 3201):
        if (i % 7 == 0 and i % 5 != 0):
            res += str(i) + ","
    print(res)


#Question 2
def question_2(num): 
    result = {}
    for i in range(1, num+1):
        result.update({i:i*i})
    print(result)


#Question 3
def question_3():
    user_input = input("Enter numbers separated by commas: ")
    input_list = user_input.split(',')
    print(list(input_list))
    print(tuple(input_list))


#Question 4
class Question4:
    inputstring = ""
    def __init__(self, inputstring):
        self.inputstring = inputstring
    def getString(self):
        return self.inputstring
    def printString(self):
        print(self.getString().upper())

def testfunc():
    question = Question4("What is")
    str1 = question.getString()
    print(str1)
    question.printString()


#Question 5
def question_5():
    var = input("Enter a comma separated sequence of nums: ")
    nums = var.split(',')
    listni = ""
    for num in nums:
        listni += factorial(int(num)) + ","
    print(listni)

def factorial(num):
    res = 1
    for i in range(2, num+1):
        res = res * i
    return str(res)

#Question 1 (Level 2)
def valAccToFormula():
    var = input("Enter comma separated D's: ")
    dlist = var.split(",")
    listni = ""
    for d in dlist:
        curr = math.sqrt((2 * int(d) * 30)/30)
        listni += (str(curr) + ",")
    print(listni)

#Question 2
def sort_words():
    var = input("Enter a comma separated sequence of words: ")
    input_list = var.split(',')
    ans = sorted(input_list)
    listni= ""
    for a in ans:
        listni += (a + ",")
    print listni

#Question 3
def divbyfive():
    var = input("Enter a comma separated 4 digit binary numbers: ")
    input_list = var.split(',')
    listni = ""
    for binary in input_list:
        var2 = int(binary, 2)
        if (var2 % 5 == 0):
            listni += (binary + ",")
    print(listni)


#Question 1 (Level 3)
def palindrome():
    var = input("Enter a string: ")
    print(True if (var==var[::-1]) else False) 

